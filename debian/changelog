starjava-dpac (1.0+2024.08.01-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + starlink-dpac-java-doc: Add Multi-Arch: foreign.

  [ Ole Streicher ]
  * Update get-orig-source to use git sparse checkout instead of svn
  * New upstream version 1.0+2024.08.01
  * Rediff patches
  * Push Standards-Version to 4.7.0. No changes needed

 -- Ole Streicher <olebole@debian.org>  Sat, 12 Oct 2024 15:59:14 +0200

starjava-dpac (1.0+2022.07.21-1) unstable; urgency=medium

  * New upstream version 1.0+2022.07.21
  * Push minversion of starjava-table to 4.1.3
  * Push Standards-Version to 4.6.1. No changes needed

 -- Ole Streicher <olebole@debian.org>  Fri, 07 Oct 2022 11:51:34 +0200

starjava-dpac (1.0+2021.01.08-1) unstable; urgency=medium

  * New upstream version 1.0+2021.01.08
  * Push Standards-Version to 4.5.1. No changes needed
  * Push dh-compat to 13
  * Add "Rules-Requires-Root: no" to d/control
  * Add versionized build-deps for starlink-table

 -- Ole Streicher <olebole@debian.org>  Fri, 15 Jan 2021 10:58:22 +0100

starjava-dpac (1.0+2020.10.01-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Submit, Repository, Repository-Browse.
  * Remove obsolete field Contact from debian/upstream/metadata (already
    present in machine-readable debian/copyright).
  * Update standards version to 4.5.0, no changes needed.
  * Replace use of deprecated $ADTTMP with $AUTOPKGTEST_TMP.

  [ Ole Streicher ]
  * New upstream version 1.0+2020.10.01
  * Rediff patches

 -- Ole Streicher <olebole@debian.org>  Fri, 06 Nov 2020 10:40:11 +0100

starjava-dpac (1.0+2019.07.12-1) unstable; urgency=low

  * New upstream version 1.0+2019.07.12. Rediff patches
  * Push Standards-Version to 4.4.0. No changes required
  * Push compat to 12. Remove d/compat
  * Add gitlab-ci for salsa

 -- Ole Streicher <olebole@debian.org>  Fri, 02 Aug 2019 11:22:15 +0200

starjava-dpac (1.0+2018.03.22-1) unstable; urgency=low

  * Initial release (Closes: #896868)

 -- Ole Streicher <olebole@debian.org>  Thu, 26 Apr 2018 08:45:43 +0200
